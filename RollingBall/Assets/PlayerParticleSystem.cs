﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PlayerParticleSystem : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particles;
    // Use this for initialization

    private void OnEnable()
    {
        particles.Stop(true);
        EventManager.StartListening("game-startup-over", ActivateParticles);
    }

    private void ActivateParticles(object obj, GameEventArgs e)
    {
        particles.Play(true);
        EventManager.StopListening("game-startup-over", ActivateParticles);
    }
}
