﻿using UnityEngine;
using System.Collections;

public class ScoreCarrier : MonoBehaviour
{
    private static ScoreCarrier instance;
    public static ScoreCarrier Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public static int Score { get; set; }
    public static int HighestScore { get; set; }
}
