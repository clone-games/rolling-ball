﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using TheCloneTeam.Infrastructure;

public class RestartLevel : MonoBehaviour
{
    public void RestartGame(string name)
    {
        GameSceneManager.LoadScene(name);
    }
}
