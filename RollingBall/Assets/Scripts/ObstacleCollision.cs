﻿using UnityEngine;
using System.Collections;

public class ObstacleCollision : MonoBehaviour
{
    private bool isEnabled;

    private void Start()
    {
        isEnabled = false;
    }

    private void OnEnable()
    {
        EventManager.StartListening("player-destroyed", SetEnabledState);
        EventManager.StartListening("on-triggers-enabled", SetEnabledState);
    }

    private void OnDisable()
    {
        EventManager.StopListening("player-destroyed", SetEnabledState);
        EventManager.StopListening("on-triggers-enabled", SetEnabledState);
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Obstacle") && isEnabled)
        {
            var e = new GameEventArgs(gameObject, col.gameObject);
            EventManager.TriggerEvent("obstacle-collision", this, e);
        }
    }

    private void SetEnabledState(object sender, GameEventArgs e)
    {
        //if (!isEnabled) isEnabled = true;
        //else isEnabled = false;
        if (sender.GetType() == typeof(MovementScript))
            isEnabled = false;
        else if (sender.GetType() == typeof(GameLogic))
            isEnabled = true;
    }
}
