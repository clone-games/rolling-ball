﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{

    private Dictionary<string, GameEvent> eventDictionary;
    private static EventManager thisInstance;

    /// <summary>
    /// Get the instance for the event manager component in the scene
    /// </summary>
    public static EventManager Instance
    {
        get
        {
            // If this instance is null
            if (!thisInstance)
            {
                // Try to find an instance of EventManager to assign to this instance
                thisInstance = FindObjectOfType(typeof(EventManager)) as EventManager;

                // If none was found
                if (!thisInstance)
                {
                    Debug.LogError("At least one EventManager should be in your scene!");
                }
                else
                {
                    // Otherwise, initialize the found eventManager
                    thisInstance.Initialize();
                }
            }

            // Return this instance
            return thisInstance;
        }
    }

    private void Initialize()
    {
        // Initialize event dictionary
        eventDictionary = new Dictionary<string, GameEvent>();
    }

    /// <summary>
    /// Attaches the listener method to an event
    /// </summary>
    /// <param name="eventName">The name of the event the listener is going to be attached to</param>
    /// <param name="listener">The listener method that is going to be attached to the event</param>
    public static void StartListening(string eventName, ListenerMethod listener)
    {
        GameEvent unityEvent;

        // Try to find a value with event name and assign the result to 
        // unity event reference if one is found
        if (Instance.eventDictionary.TryGetValue(eventName, out unityEvent))
        {
            // Assign the listener to the event
            unityEvent.AddListener(listener);
        }

        // Otherwise
        else
        {
            unityEvent = new GameEvent();                           // Initialize unity event
            unityEvent.AddListener(listener);                       // Add the listener to the event
            Instance.eventDictionary.Add(eventName, unityEvent);    // Add the event to the event dictionary
        }
    }

    /// <summary>
    /// Detaches the listener method from an event
    /// </summary>
    /// <param name="eventName">The name of the event the listener is going to be detached from</param>
    /// <param name="listener">The listener method that is going to be detached from the event</param>
    public static void StopListening(string eventName, ListenerMethod listener)
    {
        // If the instance is null (The manager is disposed of)
        // Return back
        if (!thisInstance) return;

        GameEvent unityEvent;

        // Try to get the event from the event dictionary and assign it to unity event reference if it is found
        if (Instance.eventDictionary.TryGetValue(eventName, out unityEvent))
        {
            // Remove the listener from the event
            unityEvent.RemoveListener(listener);
        }
    }

    public static void TriggerEvent(string eventName, object sender, GameEventArgs e)
    {
        GameEvent unityEvent;

        // Try to find an event with name 'eventName', and assign it to 
        // reference 'unity event' if one is found
        if (Instance.eventDictionary.TryGetValue(eventName, out unityEvent))
        {
            // Invoke event
            unityEvent.Invoke(sender, e);
        }
    }
}
