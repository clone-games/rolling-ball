﻿using UnityEngine;
using System.Collections;

public class NodeCollision : MonoBehaviour {

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            var e = new GameEventArgs(gameObject, col.gameObject);
            EventManager.TriggerEvent("player-left-node", this, e);
        }
    }
}
