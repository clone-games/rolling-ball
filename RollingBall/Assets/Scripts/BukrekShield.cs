﻿using UnityEngine;
using System.Collections;

public class BukrekShield : MonoBehaviour
{
    [SerializeField]
    private Transform canvas;

    private Transform worldCamera;
    private bool isActive;

    private void OnEnable()
    {
        EventManager.StartListening("on-triggers-enabled", SetShieldActive);
        EventManager.StartListening("game-startup-over", SetShieldActive);
        EventManager.StartListening("player-destroyed", SetShieldActive);
    }

    private void OnDisable()
    {
        EventManager.StopListening("on-triggers-enabled", SetShieldActive);
        EventManager.StopListening("game-startup-over", SetShieldActive);
        EventManager.StopListening("player-destroyed", SetShieldActive);
    }

    private void Start()
    {
        isActive = false;
        worldCamera = GameObject.Find("Main Camera").transform;
        canvas.GetComponent<Canvas>().worldCamera = worldCamera.GetComponent<Camera>();
    }

    private void Update()
    {
        canvas.LookAt(worldCamera);
    }

    private void SetShieldActive(object sender, GameEventArgs e)
    {
        if (!isActive)
            isActive = true;
        else isActive = false;
        
        canvas.gameObject.SetActive(isActive);
    }
}
