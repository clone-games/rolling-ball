﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    [SerializeField]
    private int lives;
    //[SerializeField]
    //private float startSpeed;
    [SerializeField]
    private GameObject player;
    //never Used
    //[SerializeField]
    //private GameObject level;

    private Text uiLives;
    //private bool canWatchAdvertisement;
    private bool gameOver = false;

    private void Start()
    {
        //canWatchAdvertisement = true;
        uiLives = GameObject.Find("Lives").GetComponent<Text>();
        uiLives.text = "Lives: " + lives.ToString();

        player = RollingBallProperties.Player;

        StartGame();
    }

    private void OnEnable()
    {
        EventManager.StartListening("player-destroyed", InitPlayer);
    }

    private void OnDisable()
    {
        EventManager.StopListening("player-destroyed", InitPlayer);
    }

    private void LateUpdate()
    {
        GameOver();
    }

    private void StartGame()
    {
        gameOver = false;

        // Trigger game start up
        EventManager.TriggerEvent("game-startup", this, null);
        StartCoroutine(InitPlayer(0, 5, 0));
    }

    private void InitPlayer(object sender, GameEventArgs e)
    {
        StartCoroutine(InitPlayer((float)e[0], (float)e[1], 3));
        uiLives.text = "Lives: " + --lives;
    }

    private IEnumerator InitPlayer(float waitBeforeInit, float waitAfterInit, float setTriggers)
    {
        yield return new WaitForSeconds(waitBeforeInit);
        Instantiate(player, player.transform.position, player.transform.rotation);
        StartCoroutine(GameStartupOver(waitAfterInit, 1));
    }

    private IEnumerator GameStartupOver(float wait, float setTriggers)
    {
        yield return new WaitForSeconds(wait);

        // Trigger game startup over
        EventManager.TriggerEvent("game-startup-over", this, new GameEventArgs(PlayerScoreManager.DifficultyLevel.Speed));
        yield return new WaitForSeconds(setTriggers);
        EventManager.TriggerEvent("on-triggers-enabled", this, null);
    }

    private void GameOver()
    {
        if (lives <= 0)
        {
            ScoreCarrier.Score = PlayerScoreManager.Score;
            SaveHighestScore();
            SceneManager.LoadScene("GameOver");
        }
    }

    //private void AdvertisementResult(object sender, GameEventArgs e)
    //{
    //    bool isParamValid = e != null && e[0] != null;
    //    //canWatchAdvertisement = false;

    //    if (isParamValid && System.Convert.ToBoolean(e[0]))
    //    {
    //        lives++;
    //        uiLives.text = "Lives: Last Chance!";
    //    }
    //}

    private void SaveHighestScore()
    {
        ScoreCarrier.HighestScore = 0;

        if (PlayerPrefs.HasKey("HighestScore"))
        {
            ScoreCarrier.HighestScore = PlayerPrefs.GetInt("HighestScore");
        }

        if (ScoreCarrier.HighestScore < PlayerScoreManager.Score)
        {
            PlayerPrefs.SetInt("HighestScore", PlayerScoreManager.Score);
            PlayerPrefs.Save();
        }
    }
}
