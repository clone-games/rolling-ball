﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class LevelPoolManager : MonoBehaviour, IUsageManager
{
    //never used
    //[SerializeField]
    //private int poolSize;
    [SerializeField]
    private List<PoolMetaData> poolMetas;
    [SerializeField]
    private int safeChunks;
    
    private ObjectPool inUsePool;
    private System.Random rand;

    //public LevelPoolManager()
    //{
    //    // Initialize in use pool
    //    inUsePool = new ObjectPool();

    //    // Initialize properties of the object pools
    //    InstantiateCollection(ref normalLevelObjects, normalLevelPrefab, normalLevelAmount);
    //    InstantiateCollection(ref rocketObstacles, rocketObstaclePrefab, rocketObstacleAmount);
    //    InstantiateCollection(ref laserObstacles, laserObstaclePrefab, laserObstacleAmount);
    //    InstantiateCollection(ref pipeObstacles, pipeObstaclePrefab, pipeObstacleAmount);

    //    scoreManager = GetComponent<PlayerScoreManager>();  // Get score manager
    //    rand = new System.Random();                         // Instantiate random number generator
    //}

    private void OnEnable()
    {
        EventManager.StartListening("game-startup", InitLevelPoolManager);
    }

    private void OnDisable()
    {
        EventManager.StopListening("game-startup", InitLevelPoolManager);
    }

    private void InitLevelPoolManager(object sender, GameEventArgs e)
    {
        // If any of the meta data objects lack pool name or prefab information
        if (poolMetas.Exists(meta => meta.PoolName == null || meta.Prefab == null))
        {
            // Log error
            Debug.LogError("One of the meta data objects for object pools has lacking information");
            //return;         // Return
        }

        // Initialize in use pool
        inUsePool = new ObjectPool();

        // Initialize properties of the object pools
        foreach (var metaData in poolMetas)
        {
            var relatedProperty = RollingBallProperties.GetProperty(RollingBallProperties.Properties, metaData.PoolName);

            if (relatedProperty != null)
            {
                metaData.Prefab = relatedProperty.Value;
            }

            InstantiateCollection(ref metaData.Pool, metaData.Prefab, metaData.PoolSize);
        }
        
        rand = new System.Random();                         // Instantiate random number generator

        EventManager.TriggerEvent("pools-generated", this, new GameEventArgs());
    }

    private void InstantiateCollection(ref ObjectPool pool, GameObject obj, int count)
    {
        pool = new ObjectPool();

        for (int i = 0; i < count; i++)
        {
            var gameObject = Instantiate(obj);
            gameObject.SetActive(false);
            pool.AddObject(gameObject);
        }
    }

    GameObject IUsageManager.FreeObject()
    {
        var obj = inUsePool[0];
        inUsePool.RemoveObject(obj);
        obj.SetActive(false);

        return obj;
    }

    GameObject IUsageManager.UseObject()
    {
        int prob = 0;

        if (inUsePool.Count > safeChunks)
        {
            prob = rand.Next(0, 100);
        }
        
        return SwitchAndUseModel(prob);
    }

    private GameObject SwitchAndUseModel(int prob)
    {
        // Create an empty game object
        GameObject obj = null;

        var level = PlayerScoreManager.DifficultyLevel;     // Get difficulty level
        var type = level.LevelModels[level[prob]];          // Calculate the type of obstacle to be used with the probablity given

        // Get meta data for the selected type of obstacle pool
        var currentMeta = poolMetas.Find(meta => meta.ObstacleType == type);
        
        // If the meta data is not null
        if (currentMeta != null)
        {
            // Loop the pool
            for (int i = 0; i < currentMeta.Pool.Count; i++)
            {
                // Continue till an object that is not active in hierarchy is found
                if (currentMeta.Pool[i].activeInHierarchy) continue;
                obj = currentMeta.Pool[i];          // Get the found object
                break;                              // Halt the loop
            }
        }

        // Add selected obstacle to the in use pool (Use it)
        inUsePool.AddObject(obj);
        obj.SetActive(true);            // Set the used object active (Date : 14.09.2016 - Changed to SetActiveRecursively for activating all childs)
        return obj;                     // Return the reference for the used object
    }
}
