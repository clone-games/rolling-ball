﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ObjectPool
{
    private LinkedList<GameObject> objects;

    /// <summary>
    /// Creates a new Object Pool
    /// </summary>
    public ObjectPool()
    {
        objects = new LinkedList<GameObject>();
    }

    /// <summary>
    /// Adds the specified object to the underlying collection
    /// </summary>
    /// <param name="obj">The object to be added</param>
    public void AddObject(GameObject obj)
    {
        // If the obj reference is already added to the list
        foreach (var o in objects)
        {
            if (ReferenceEquals(o, obj)) return;
        }

        objects.AddLast(obj);
    }

    /// <summary>
    /// Removes an object from the underlying collection
    /// </summary>
    /// <param name="obj">The object to be removed</param>
    public void RemoveObject(GameObject obj)
    {
        objects.Remove(obj);
    }
    
    /// <summary>
    /// Clears the collection
    /// </summary>
    public void Clear()
    {
        while (objects.Count > 0)
        {
            objects.RemoveLast();
        }
    }

    /// <summary>
    /// Get the number of elements in the underlying collection
    /// </summary>
    public int Count
    {
        get { return objects.Count; }
    }

    /// <summary>
    /// Get an element at the ith index from the underlying collection
    /// </summary>
    /// <param name="i">The index of the object to be returned</param>
    /// <returns>The object at the specified index in the underlying collection</returns>
    public GameObject this[int i]
    {
        get { return objects.ElementAt(i); }
    }
}
