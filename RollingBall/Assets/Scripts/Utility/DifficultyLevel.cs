﻿using UnityEngine;
using System.Collections;

public class DifficultyLevel : MonoBehaviour
{
    public string Name;                 // Determines the name of the difficulty level
    public int StartLimit;              // Determines the starting point of the difficulty level
    public int Multiplier;              // Determines the multiplier for score calculation
    public float Speed;                 // Determines the speed of the player
    public int BetweenIndices;          // Determines range of models to be used
    public ObstacleType[] LevelModels;  // A collection of integers that will be used to determine the types of level models that will be used
    public int[] ModelProbablities;     // Rendering probablity of each model in the above collection

    public int this[int i]
    {
        get
        {
            int index = 0;

            for (int j = 0; j < LevelModels.Length; j++)
            {
                int previousIndex = Mathf.Clamp(j - 1, 0, ModelProbablities.Length);

                if (i > ModelProbablities[previousIndex] && i <= ModelProbablities[j])
                {
                    return j;
                }
            }

            return index;
        }
    }
}
