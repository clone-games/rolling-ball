﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

/// <summary>
/// This is the base class for the handmade custom event system
/// Thus the game event args class is created as a partial class 
/// so that it can be expanded without using inheritance
/// </summary>
public partial class GameEventArgs
{
    // Holds the arguements the event is going to carry
    private object[] arguements;

    /// <summary>
    /// Creates a new instance of GameEventArgs class and adds the
    /// arguements that are passed as arguements to the object
    /// </summary>
    /// <param name="args">A number of arguements that are intended for
    /// the event to carry</param>
    public GameEventArgs(params object[] args)
    {
        arguements = args;      // Assign arguements
    }

    /// <summary>
    /// Assign args to the event args instance
    /// </summary>
    /// <param name="args">An array of arguements to assign</param>
    public void AssignArgs(params object[] args)
    {
        arguements = args;      // Assign arguements
    }

    /// <summary>
    /// Assign args to the event args instance
    /// </summary>
    /// <param name="e">An event args object that holds the args</param>
    public void AssignArgs(GameEventArgs e)
    {
        arguements = e.arguements;  // Assign arguements
    }

    /// <summary>
    /// Get or set the arguements of the game event args object
    /// </summary>
    /// <param name="i">The index to be accessed</param>
    /// <returns>The i'th element in the arguements collection</returns>
    public object this[int i]
    {
        get { return arguements[i]; }
        set { arguements[i] = value; }
    }
}
