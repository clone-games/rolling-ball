﻿using UnityEngine;
using System.Collections;

public class PoolMetaData : MonoBehaviour
{
    public ObstacleType ObstacleType;
    public string PoolName;
    public ObjectPool Pool;
    public GameObject Prefab;
    public int PoolSize;
}

public enum ObstacleType
{
    Base,
    Laser,
    Pipe,
    Fan,
    Colon,
    Jump,
    Gate,
    Valve
}