﻿using UnityEngine;
using System.Collections;
using System;

public class ObjectPoolManager : MonoBehaviour, IUsageManager {
    //Never Used
    //[SerializeField]
    //private GameObject[] objects;
    [SerializeField]
    private GameObject basePool;
    private ObjectPool inUsePool;
    private ObjectPool availablePool;

    private System.Random rand;

    // Use this for initialization
    private void Start()
    {
        inUsePool = new ObjectPool();
        availablePool = new ObjectPool();
        rand = new System.Random();
        
        foreach (Transform obj in basePool.transform)
        {
            availablePool.AddObject(obj.gameObject);
        }
    }

    GameObject IUsageManager.UseObject()
    {
        int i = rand.Next(0, availablePool.Count);
        var a = availablePool[i];
        //var a = availablePool[availablePool.Count - 1];
        availablePool.RemoveObject(a);
        inUsePool.AddObject(a);

        return a;
    }

    GameObject IUsageManager.FreeObject()
    {
        var a = inUsePool[0];
        availablePool.AddObject(a);
        inUsePool.RemoveObject(a);

        return a;
    }
}
