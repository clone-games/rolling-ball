﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public delegate void ListenerMethod(object sender, GameEventArgs e);

/// <summary>
/// This class is intended to hold necessary event data
/// and carry it accross the event system. To be able to freely
/// expand it later without using inheritance, it is created as
/// a partial class
/// </summary>
public partial class GameEvent
{
    private event ListenerMethod thisEvent;

    public void AddListener(ListenerMethod listener)
    {
        thisEvent += listener;
    }

    public void RemoveListener(ListenerMethod listener)
    {
        thisEvent -= listener;
    }

    public void Invoke(object sender, GameEventArgs e)
    {
        thisEvent.Invoke(sender, e);
    }
}
