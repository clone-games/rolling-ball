﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreAtGameOver : MonoBehaviour
{
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private Text highestScoreText;

    private void Update()
    {
        scoreText.text = "Score : " + ScoreCarrier.Score;
        highestScoreText.text = "Highest Score : " + ScoreCarrier.HighestScore;
    }

    public void SaveScoreToDatabase(string user, int score)
    {

    }
}
