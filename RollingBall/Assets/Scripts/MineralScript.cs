﻿using UnityEngine;
using System.Collections;

public class MineralScript : MonoBehaviour
{
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            var e = new GameEventArgs(gameObject, col.gameObject);
            EventManager.TriggerEvent("player-get-mineral", this, e);
            gameObject.SetActive(false);    
        }
    }

    //   [SerializeField]
    //   private Transform direction;            // This is the direction of the raycast
    //   [SerializeField]
    //   private Quaternion rotateValue;         // This value indicates how much a mineral should be rotated
    //   [SerializeField]
    //   private float castRange;                // Range of the ray to be cast
    //   [SerializeField]
    //   private float distanceFromGround;       // This value indicates 
    //   [SerializeField]
    //   private Transform referencePosition;    // This position defines from where the rays will be cast
    //   [SerializeField]
    //   private AnimationCurve curve;           // A curve indicating how often should minerals appear

    //   private PlayerScoreManager scoreManager;
    //   private Vector3 position;
    //   private bool isReady;

    //   private void OnEnable()
    //   {
    //       EventManager.StartListening("score-manager-initialized", InitMineralScript);
    //   }

    //   private void OnDisable()
    //   {
    //       EventManager.StopListening("score-manager-initialized", InitMineralScript);
    //   }

    //   private void Start()
    //   {
    //       isReady = false;
    //   }

    //// Use this for initialization
    //private void InitMineralScript(object sender, GameEventArgs e)
    //   {
    //       scoreManager = sender as PlayerScoreManager;
    //       isReady = true;
    //   }

    //// Update is called once per frame
    //void Update ()
    //   {

    //}

    //   private Vector3 CalculatePossiblePosition()
    //   {
    //       var ray = new Ray(position, direction.position);
    //       RaycastHit hit;

    //       // If the ray hits the level
    //       if (Physics.Raycast(ray, out hit, castRange))
    //       {
    //           var distanceVector = (hit.point - position).normalized * distanceFromGround;                // Calculate distance vector
    //           return distanceVector + hit.point;          // Return calculated mineral position
    //       }

    //       // Return 0 vector
    //       return Vector3.zero;
    //   }

    //   private void LoadMineralPrefab()
    //   {

    //   }
}
