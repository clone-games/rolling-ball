﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {
    //never used
    //[Range(0, 30)]
    //[SerializeField]
    //private float objectFreeInterval;
    [SerializeField]
    private Transform renderPosition;
    [SerializeField]
    private int serializeUnitsOnStartup;
    [SerializeField]
    private Transform level;

    // 07.06.2016 -> Added for object pool manager support
    [SerializeField]
    private int noOfChunksBeforeRepos;
    private IUsageManager iPoolManager;
    private int chunksPassed;

    private System.Random rand;

    private void OnEnable()
    {
        // Add event listeners
        EventManager.StartListening("pools-generated", InitGenerator);
        EventManager.StartListening("pools-generated", GenerateFull);

        // 07.06.2016 -> Added for object pool manager support
        EventManager.StartListening("player-left-node", FreeChunk);
        EventManager.StartListening("player-left-node", UseChunk);
    }

    private void OnDisable()
    {
        // Remove event listeners
        EventManager.StopListening("pools-generated", InitGenerator);
        EventManager.StopListening("pools-generated", GenerateFull);

        // 07.06.2016 -> Added for object pool manager support
        EventManager.StopListening("player-left-node", FreeChunk);
        EventManager.StopListening("player-left-node", UseChunk);
    }

    /// <summary>
    /// Initializes the level generator
    /// </summary>
    private void InitGenerator(object sender, GameEventArgs e)
    {
        rand = new System.Random();
        iPoolManager = GetComponent<LevelPoolManager>();

        chunksPassed = 0;
    }

    /// <summary>
    /// Generates a level that can be played on
    /// </summary>
    private void GenerateFull(object sender, GameEventArgs e)
    {
        for (int i = 0; i < serializeUnitsOnStartup; i++)
        {
            UseChunk(sender, e);
        }

        // Trigger event
        EventManager.TriggerEvent("game-startup-over", this, new GameEventArgs(Time.time));
    }

    private void UseChunk(object sender, GameEventArgs e)
    {
        var shape = iPoolManager.UseObject();
        shape.transform.position = renderPosition.position;
        shape.transform.rotation = Quaternion.Euler(0, 0, (rand.Next(64) * 45) + 22.5f) * level.rotation;
        shape.transform.parent = level;

        renderPosition.position += Vector3.forward * 3;
    }

    private void FreeChunk(object sender, GameEventArgs e)
    {
        if (chunksPassed < noOfChunksBeforeRepos)
        {
            chunksPassed++;
        }
        else
        {
            iPoolManager.FreeObject();
        }
    }
}
