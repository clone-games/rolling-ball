﻿using UnityEngine;
using System.Collections;

public interface IUsageManager
{
    GameObject UseObject();
    GameObject FreeObject();
}
