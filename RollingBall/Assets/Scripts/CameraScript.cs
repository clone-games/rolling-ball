﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    [SerializeField]
    private Vector3 offset;
    [Range(0, 5)]
    [SerializeField]
    private float followSpeed;
    [Range(0, 1)]
    [SerializeField]
    private float smoothFactor;
    [SerializeField]
    private Vector3 cameraStartPos;
    [SerializeField]
    private Vector3 cameraLerpOffset;

    private Transform gameCamera;
    //NEVER USED
    //private Vector3 cameraPosition;
    //private Vector3 offsetLeft;
    //private Vector3 offsetRight;
    private bool lerpAway;

    private void OnEnable()
    {
        EventManager.StartListening("game-startup-over", SetLerpAway);
        EventManager.StartListening("level-rotated", SetRotation);
    }

    private void OnDisable()
    {
        EventManager.StopListening("game-startup-over", SetLerpAway);
        EventManager.StopListening("level-rotated", SetRotation);
    }

	// Use this for initialization
	private void Start ()
    {
        // Get main camera
        gameCamera = GameObject.Find("Main Camera").transform;
        gameCamera.position = cameraStartPos;   // Assign start position

        Mathf.Clamp(smoothFactor, 0, 1);        // Clamp smooth value between 0 and 1
        smoothFactor = 1 / smoothFactor;        // SmoothFactor will ve 1 / smooth value

        lerpAway = false;
    }
	
	// Update is called once per frame
	private void LateUpdate()
    {
        // Follow the player
        if (!lerpAway)
        {
            gameCamera.position = Vector3.Lerp(gameCamera.position,
                                            transform.position + offset,
                                            Time.deltaTime * smoothFactor * followSpeed);
        }
        else
        {
            gameCamera.position = Vector3.Lerp(gameCamera.position,
                                            transform.position + cameraLerpOffset,
                                            Time.deltaTime * smoothFactor * followSpeed);
        }

        // Look at the player
        gameCamera.LookAt(transform);
    }

    private void SetLerpAway(object sender, GameEventArgs e)
    {
        lerpAway = true;
    }

    private void SetRotation(object sender, GameEventArgs e)
    {
        cameraLerpOffset = new Vector3(1.5f * (float)e[0], cameraLerpOffset.y, cameraLerpOffset.z);
    }
}
