﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GamePause : MonoBehaviour
{

    [SerializeField]
    private Sprite button_pause;
    [SerializeField]
    private Sprite button_start;
    [SerializeField]
    private GameObject pauseScreen;
    [SerializeField]
    private GameObject pauseButton;

    private bool isPaused;

    private void OnEnable()
    {
        EventManager.StartListening("player-destroyed", HideButon);
        EventManager.StartListening("game-startup-over", ShowButton);
    }

    private void OnDisable()
    {
        EventManager.StopListening("player-destroyed", HideButon);
        EventManager.StopListening("game-startup-over", ShowButton);
    }

    void Start () {
        isPaused = false;
    }
	
	public void PauseGame()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
      
        if (isPaused)
        {
            if (player != null)
            {
                player.GetComponent<MovementScript>().enabled = true;
            }
            Time.timeScale = 1;
            isPaused = false;
            //this.GetComponent<Image>().overrideSprite = button_pause;
            pauseButton.GetComponent<Image>().overrideSprite = button_pause;
            pauseScreen.SetActive(false);
        } 

        else
        {
            if(player != null)
            {
                player.GetComponent<MovementScript>().enabled = false;
            }
            Time.timeScale = 0;
            isPaused = true;
            //this.GetComponent<Image>().overrideSprite = button_start;
            pauseButton.GetComponent<Image>().overrideSprite = button_start;
            pauseScreen.SetActive(true);
        }      
	}

    private void HideButon(object sender, GameEventArgs e)
    {
        pauseButton.SetActive(false);
    }

    private void ShowButton(object sender, GameEventArgs e)
    {
        pauseButton.SetActive(true);
    }
}
