﻿using UnityEngine;
using System.Collections;

public class MissileScript : MonoBehaviour
{
    [SerializeField]
    private float missileSpeed;
    [SerializeField]
    private float explosionRadius;
    [SerializeField]
    private float explosionForce;
    [SerializeField]
    private GameObject detonator;
    [SerializeField]
    private float explosionLife;

    private Rigidbody rigid;

    private void Start()
    {
        rigid = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision col)
    {
        var hitTargets = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (var coll in hitTargets)
        {
            if (coll.gameObject.GetComponent<Rigidbody>() != null)
            {
                coll.gameObject.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position, explosionRadius);
            }
        }

        var pos = transform.position;

        Destroy(gameObject);
    }

    private void Update()
    {
        rigid.velocity = transform.forward * missileSpeed;
    }
}
