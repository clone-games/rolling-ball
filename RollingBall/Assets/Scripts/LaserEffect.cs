﻿using UnityEngine;
using System.Collections;

public class LaserEffect : MonoBehaviour {
    [SerializeField]
    Transform startPosition;
    [SerializeField]
    Transform endPosition;
    LineRenderer laserRenderer;


	// Use this for initialization
	void Start () {
        laserRenderer = GetComponent<LineRenderer>();
        laserRenderer.SetWidth(1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
        laserRenderer.SetPosition(0, startPosition.position);
        laserRenderer.SetPosition(1, endPosition.position);
	}
}
