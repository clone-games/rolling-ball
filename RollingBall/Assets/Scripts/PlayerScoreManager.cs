﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerScoreManager : MonoBehaviour
{
    [SerializeField]
    private DifficultyLevel[] difficulties;
    [SerializeField]
    private int mineralScore;

    private Text scoreText;
    private Text difficultyText;
    //never used
    //private bool isEnabled;
    private int difficultyIndex;
    private int score;

    private static PlayerScoreManager instance;
    public static PlayerScoreManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnLevelWasLoaded()
    {
        //if (SceneManager.GetActiveScene().name == "Scene1")
        //{
        //    InitManager();
        //}
        //else
        //{
        //    isEnabled = false;
        //}
    }

    private void Start()
    {
        //isEnabled = false;
        InitManager();
    }

    private void InitManager()
    {
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        difficultyText = GameObject.Find("Difficulty").GetComponent<Text>();
        //isEnabled = true;

        if (difficulties == null)
        {
            difficulties = new DifficultyLevel[]
            {
                new DifficultyLevel() { Name = "Normal", StartLimit = 0, Multiplier = 1, Speed = 10 }
            };
        }
        difficulties.OrderBy(item => item.StartLimit);

        difficultyIndex = 0;
        score = 0;

        EventManager.TriggerEvent("score-manager-initalized", this, null);
    }

    private void OnEnable()
    {
        EventManager.StartListening("player-left-node", CalculateScore);
        EventManager.StartListening("player-get-mineral", MineralScore);
    }

    private void OnDisable()
    {
        EventManager.StopListening("player-left-node", CalculateScore);
        EventManager.StopListening("player-get-mineral", MineralScore);
    }

    private void Update()
    {
        scoreText.text = "Score: " + score;
        difficultyText.text = "Difficulty: " + DifficultyLevel.Name;
    }

    public void CalculateScore(object sender, GameEventArgs e)
    {
        if (difficultyIndex < difficulties.Length - 1 
            && score > difficulties[difficultyIndex + 1].StartLimit)
        {
            difficultyIndex++;

            // Difficulty changed
            EventManager.TriggerEvent("difficulty-changed", this, new GameEventArgs(difficulties[difficultyIndex]));
        }

        score += difficulties[difficultyIndex].Multiplier;
    }

    //14.09.2016
    public void MineralScore(object sender, GameEventArgs e)
    {
        score += mineralScore;
    }

    public static DifficultyLevel DifficultyLevel
    {
        get { return Instance.difficulties[Instance.difficultyIndex]; }
    }

    public static DifficultyLevel NextLevel
    {
        get
        {
            if (Instance.difficulties.Length > Instance.difficultyIndex + 1)
            {
                return Instance.difficulties[Instance.difficultyIndex + 1];
            }

            return null;
        }
    }

    public static DifficultyLevel PreviousLevel
    {
        get
        {
            if (0 < Instance.difficultyIndex - 1)
            {
                return Instance.difficulties[Instance.difficultyIndex - 1];
            }

            return null;
        }
    }

    public static int Score
    {
        get { return Instance.score; }
    }
}
