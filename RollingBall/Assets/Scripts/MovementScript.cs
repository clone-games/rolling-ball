﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class MovementScript : MonoBehaviour
{
    //[Range(0, 20)]
    //[SerializeField]
    private float speed;
    private float initialSpeed;
    private bool isEnabled;
    //Never used
    //private bool isImmortal;

    private Transform level;
    private Vector2 touchOrigin;

	private void OnEnable()
    {
        EventManager.StartListening("game-startup-over", InitMovement);
        EventManager.StartListening("difficulty-changed", DifficultyChanged);
        EventManager.StartListening("obstacle-collision", ResetMovement);
    }

    private void OnDisable()
    {
        EventManager.StopListening("game-startup-over", InitMovement);
        EventManager.StopListening("difficulty-changed", DifficultyChanged);
        EventManager.StopListening("obstacle-collision", ResetMovement);
    }

    private void Start()
    {
        speed = 0;
        isEnabled = false;

        level = GameObject.FindGameObjectWithTag("GeneratedChunks").transform;
        touchOrigin = -Vector2.one;

        if (!level)
        {
            Debug.LogError("There must be a game object tagged \"Level\" in the scene");
        }
    }

    private void Update()
    {
        if (isEnabled)
        {
            Move();
        }
    }

    private void Move()
    {
        level.position -= level.forward * speed * Time.deltaTime;
        float horizontal = 0f;

#if UNITY_STANDALONE || UNITY_WEBPLAYER
        if (Input.gyro.enabled)
        {
            Gyroscope gyro = Input.gyro;
            horizontal = gyro.gravity.x;
        }
        else
        {
            horizontal = Input.GetAxis("Horizontal");
        }

#else
        if (Input.touchCount > 0)
        {
            Touch myTouch = Input.touches[0];

            if (myTouch.phase == TouchPhase.Moved||myTouch.phase==TouchPhase.Stationary)
            {
                touchOrigin = myTouch.position;
                
                if(touchOrigin.x>0 && touchOrigin.x < Screen.width/2)
                {
                    horizontal = -1;
                }
                else
                {
                    horizontal = 1;
                }
            }
            else
            {
                Gyroscope gyro = Input.gyro;
                horizontal = gyro.gravity.x;
            }
        }

#endif
        // Rotate level
        level.Rotate(level.forward, -horizontal * speed / 4);
        EventManager.TriggerEvent("level-rotated", this, new GameEventArgs(-horizontal));
    }
    
    private void InitMovement(object sender, GameEventArgs e)
    {
        initialSpeed = speed = (float)e[0];
        isEnabled = true;
    }

    private void OnPlayerDeath(object sender, GameEventArgs e)
    {
        //StartCoroutine(ResetMovement(e));
    }

    private void ResetMovement(object sender, GameEventArgs e)
    {
        speed = 0;              // Reset player speed
        isEnabled = false;      // Disable the script

        EventManager.TriggerEvent("player-destroyed", this, new GameEventArgs(1.5f, 3f));

        Destroy(gameObject);
    }

    private void DifficultyChanged(object sender, GameEventArgs e)
    {
        speed = ((DifficultyLevel)e[0]).Speed;
    }
}
