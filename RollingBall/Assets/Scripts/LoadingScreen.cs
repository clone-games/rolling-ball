﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TheCloneTeam.Infrastructure;
using UnityEngine.SceneManagement;

namespace TheCloneTeam.Infrastructure
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField]
        private Text loadingText;
        [SerializeField]
        private Text continueText;

        [Header("Loading Bar")]
        [SerializeField]
        private Image progressImage;
        [Range(0.001f, 1f)]
        [SerializeField]
        private float speed;
        [SerializeField]
        private bool clockWise;

        private void Start()
        {
            progressImage.fillClockwise = clockWise;
        }

        private void Update()
        {
            loadingText.color = new Color(loadingText.color.r, loadingText.color.g, loadingText.color.b, Mathf.PingPong(Time.time, 1));
            ProgressBarAnimation();

            if (!GameSceneManager.AutoActivationEnabled)
            {
                continueText.color = new Color(continueText.color.r, continueText.color.g, continueText.color.b, Mathf.PingPong(Time.time, 1));
            }
        }

        private void ProgressBarAnimation()
        {
            if (clockWise && progressImage.fillAmount < 1f)
            {
                progressImage.fillAmount += speed;
            }
            else if (!clockWise && progressImage.fillAmount > 0f)
            {
                progressImage.fillClockwise = false;
                progressImage.fillAmount -= speed;
            }
            else
            {
                clockWise = !clockWise;
                progressImage.fillClockwise = clockWise;
            }
        }
    }
}