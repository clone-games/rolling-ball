﻿using UnityEngine;
using System.Collections.Generic;

namespace TheCloneTeam.Infrastructure
{
    [System.Serializable]
    public class GameScene
    {
        public string Name;
        public int Value;
    }
}