﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine.UI;

namespace TheCloneTeam.Infrastructure
{
    public class GameSceneManager : MonoBehaviour
    {
        [Header("Declare Scenes")]
        [SerializeField]
        private GameScene[] scenes;

        [Header("Load Options")]
        [SerializeField]
        private bool useLoadingScreen;
        [SerializeField]
        private GameObject loadingScreen;
        [SerializeField]
        private bool autoActivateScene;
        [SerializeField]
        private bool progressBarOverride;

        [Header("Auto Activation Properties")]
        [SerializeField]
        private Text loadingText;
        [SerializeField]
        private Text continueText;
        [SerializeField]
        private Image progressImage;

        private List<GameScene> scenesList;
        private static GameSceneManager thisManager;
        private GameScene currentScene;
        private GameScene sceneToLoad;
        private AsyncOperation operation;


        // ------------------------------------------------------------------------------------------------------------------------------ //
        //                                                          UNITY EVENTS                                                          //
        // ------------------------------------------------------------------------------------------------------------------------------ //


        private void Awake()
        {
            if (!thisManager)
            {
                thisManager = this;
                DontDestroyOnLoad(gameObject);
                Initialize();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnSceneWasLoaded()
        {
            loadingScreen.SetActive(false);
        }


        // ------------------------------------------------------------------------------------------------------------------------------ //
        //                                                            METHODS                                                             //
        // ------------------------------------------------------------------------------------------------------------------------------ //


        private void Initialize()
        {
            if (useLoadingScreen)
            {
                DontDestroyOnLoad(loadingScreen.transform.parent.gameObject);
                Instance.loadingScreen.transform.parent.GetComponents<MonoBehaviour>()
                                                       .ToList()
                                                       .ForEach(comp => comp.enabled = false);
            }

            if (scenes.Length > 0)
            {
                Scenes = scenes == null ? new List<GameScene>() : scenes.ToList();
            }
        }

        public static void LoadScene(string sceneName)
        {
            if (!IsLoading)
            {
                SceneToLoad = Scenes.Find(scene => scene.Name == sceneName);

                // Enable script components
                Instance.loadingScreen.transform.parent.GetComponents<MonoBehaviour>()
                                                       .ToList()
                                                       .ForEach(comp => comp.enabled = true);
                Instance.loadingScreen.SetActive(true);              // Show the loading screen
                Instance.continueText.gameObject.SetActive(false);   // Hide the continue text

                if (SceneToLoad != null)
                {
                    Instance.StartCoroutine(Instance.Load());
                }
            }
        }

        private IEnumerator Load()
        {
            yield return new WaitForSeconds(1);

            Operation = SceneManager.LoadSceneAsync(SceneToLoad.Value);
            Operation.allowSceneActivation = false;

            // While the scene is loading
            while (IsLoading)
            {
                // Update the loading screen visuals
                UpdateLoadingVisuals();

                // If operation is complete and waiting for scene activation
                if (Operation.progress >= 0.9f)
                {
                    // If auto activate scene mode is enabled
                    if (autoActivateScene)
                    {
                        // Allow scene activation
                        Operation.allowSceneActivation = true;
                    }
                    else
                    {
                        loadingText.gameObject.SetActive(false);        // Hide 'Loading...' text
                        progressImage.gameObject.SetActive(false);      // Hide progress bar
                        continueText.gameObject.SetActive(true);        // Show 'Press any key to continue...' text

                        // Otherwise if any key is pressed
                        if (Input.anyKey)
                        {
                            // Allow scene activation
                            Operation.allowSceneActivation = true;
                        }
                    }
                }

                yield return null;
            }

            // Reset the progress bar
            progressImage.fillAmount = 0;

            CurrentScene = SceneToLoad;     // Set the current scene as the loaded scene
            SceneToLoad = null;             // Set the scene to load null

            // Hide the loading screen
            loadingScreen.SetActive(false);
            // Disable script components
            Instance.loadingScreen.transform.parent.GetComponents<MonoBehaviour>()
                                                   .ToList()
                                                   .ForEach(comp => comp.enabled = false);
        }

        private void UpdateLoadingVisuals()
        {
            if (!progressBarOverride)
            {
                if (Operation.progress >= 0.9f)
                {
                    progressImage.fillAmount = 1;
                }
                else
                {
                    progressImage.fillAmount = Operation.progress;
                }
            }
        }


        // ------------------------------------------------------------------------------------------------------------------------------ //
        //                                                           PROPERTIES                                                           //
        // ------------------------------------------------------------------------------------------------------------------------------ //


        public static GameSceneManager Instance
        {
            get { return thisManager; }
        }
        public static List<GameScene> Scenes
        {
            get { return Instance.scenesList; }
            protected set { Instance.scenesList = value; }
        }
        public static GameScene CurrentScene
        {
            get { return Instance.currentScene; }
            protected set { Instance.currentScene = value; }
        }
        public static GameScene SceneToLoad
        {
            get { return Instance.sceneToLoad; }
            protected set { Instance.sceneToLoad = value; }
        }
        public static AsyncOperation Operation
        {
            get { return Instance.operation; }
            protected set { Instance.operation = value; }
        }
        public static GameObject LoadingScreen
        {
            get { return Instance.loadingScreen; }
            protected set { Instance.loadingScreen = value; }
        }
        public static bool IsLoading
        {
            get { return Operation != null ? !Operation.isDone : false; }
        }
        public static bool AutoActivationEnabled
        {
            get { return Instance.autoActivateScene; }
        }
    }
}
