﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{

    [SerializeField]
    private Sprite button_mute;
    [SerializeField]
    private Sprite button_unmute;
    [SerializeField]
    private GameObject soundSystem;

    private AudioSource audioSource;

    void Start()
    {
        if (soundSystem != null)
        {
            audioSource = soundSystem.GetComponent<AudioSource>();
        }
    }

    public void MuteControl()
    {
        if (audioSource.mute)
        {
            audioSource.mute = false;
            this.GetComponent<Image>().overrideSprite = button_unmute;
        }
        else
        {
            audioSource.mute = true;
            this.GetComponent<Image>().overrideSprite = button_mute;
        }
    }
}
