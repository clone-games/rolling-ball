﻿using UnityEngine;
using System.Collections;

public class cam : MonoBehaviour
{
    [SerializeField]
    private Transform target;
    [SerializeField]
    private float speedFactor;
    [SerializeField]
    private Animation animatorTarget1;
    [SerializeField]
    private Animation animatorTarget2;
    [SerializeField]
    private float waitBeforeMovement;

    // Use this for initialization
    private void Start ()
    {
        
	}
	
	// Update is called once per frame
	private void Update ()
    {
        transform.position = Vector3.Slerp(transform.position, target.position, speedFactor * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, target.rotation, speedFactor * Time.deltaTime);
	}

    public void SetMount(Transform newTarget)
    {
        if (animatorTarget1 != null && animatorTarget2 != null)
        {
            animatorTarget1.Play();
            animatorTarget2.Play();
        }

        StartCoroutine(Sleep(waitBeforeMovement, newTarget));
    }

    public void SetAnim1(Transform anim1)
    {
        animatorTarget1 = anim1.GetComponent<Animation>();
    }

    public void SetAnim2(Transform anim2)
    {
        animatorTarget2 = anim2.GetComponent<Animation>();
    }

    public IEnumerator Sleep(float seconds, Transform newTarget)
    {
        yield return new WaitForSeconds(seconds);

        target = newTarget;
    } 
}
