﻿using UnityEngine;
using System.Collections;

public interface IHasPropertyMetaData
{
    void InitPropertyFromMeta();
}
