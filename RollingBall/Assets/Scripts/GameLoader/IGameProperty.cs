﻿using UnityEngine;
using System.Collections;
using System;

public interface IGameProperty
{
    string Name { get; set; }
    GameObject Value { get; set; }
}
