using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

public class GameProperties<T> : MonoBehaviour
    where T : class
{
    private UnityAction specializedLoader;
    private Dictionary<T, IGameProperty> properties;
    private GameObject mainCamera;
    private GameObject player;
	
	public static GameProperties<T> Instance;

    private void Awake()
    {
        Instance = this;
        this.Initialize();
    }

	public static IGameProperty GetProperty(Dictionary<T, IGameProperty> propertyDictionary, T key)
	{
		IGameProperty obj;
		propertyDictionary.TryGetValue(key, out obj);
		return obj;
	}

    public static void AddOrChangeProperties(Dictionary<T, IGameProperty> propertydictionary, params IGameProperty[] properties)
    {
        foreach (var property in properties)
        {
            if (property.Name is T)
            {
                var key = property.Name as T;
                propertydictionary.Add(key, property);
            }
        }
    }

	public static void AddOrChangeProperty(Dictionary<T, IGameProperty> propertyDictionary, T key, IGameProperty value)
    {
		if (propertyDictionary.ContainsKey(key))
		{
			propertyDictionary[key] = value;
		}
		else 
		{
			Debug.LogWarning("Property with key: " + key.ToString() + " not found, adding property to the collection");
			propertyDictionary.Add(key, value);
		}
	}
	
	public static void LoadProperties()
    {
        if (Instance.specializedLoader != null)
            Instance.specializedLoader();

        foreach (var kvp in Properties)
		{
			var iLoadable = kvp.Value as ILoadable;
			iLoadable.LoadData();
		}
    }

    [System.Obsolete("This method is obsolete, don't use it, it might cause problems!!!")]
    public static void SetSpecializedLoader(UnityAction loader)
    {
        Instance.specializedLoader = loader;
    }

    protected virtual void Initialize()
    {
        // Initialize all collections
        Properties = new Dictionary<T, IGameProperty>();
    }

    protected virtual void SetPlayer()
    {
        IGameProperty playerObj = null;

        if (Properties.TryGetValue("player" as T, out playerObj))
        {
            player = playerObj.Value;
        }
    }

    protected virtual void SetMainCamera()
    {
        IGameProperty mainCameraObj = null;

        if (Properties.TryGetValue("main-camera" as T, out mainCameraObj))
        {
            mainCamera = mainCameraObj.Value;
        }
    }
    
    public static Dictionary<T, IGameProperty> Properties
    {
        get { return Instance.properties; }
        protected set { Instance.properties = value; }
    }

    public static GameObject MainCamera
    {
        get { return Instance.mainCamera; }
        set { Instance.mainCamera = value; }
    }

	public static GameObject Player
    {
        get { return Instance.player; }
        set { Instance.player = value; }
    }
}