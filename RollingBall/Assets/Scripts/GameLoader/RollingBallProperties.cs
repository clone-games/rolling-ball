﻿using UnityEngine;
using System.Collections;
using System;

public class RollingBallProperties : GameProperties<string>, IHasPropertyMetaData
{
    private static string modelPath = "Model/";
    private static string themeName = "Scifi";

    protected override void Initialize()
    {
        base.Initialize();
        InitPropertyFromMeta();

        // Load Properties here
        LoadProperties();
        SetMainCamera();
        SetPlayer();
    }

    public void InitPropertyFromMeta()
    {
        // Add properties to be loaded
        AddOrChangeProperties(Properties,

            // Player
            //new LoadableProperty()
            //{
            //    Name = "player",
            //    Slug = "Player",
            //    DataPath = modelPath + themeName
            //},

            // Player Copy
            new LoadableProperty()
            {
                Name = "player",
                Slug = "Player_Bukrek 1",
                DataPath = modelPath + themeName
            },

            // Main Camera
            new LoadableProperty()
            {
                Name = "main-camera",
                Slug = "mainCamera",
                DataPath = @"Model"
            },

            // Level Base
            new LoadableProperty()
            {
                Name = "level-base",
                Slug = "Level_Base",
                DataPath = modelPath + themeName
            },

            // Level Colon
            new LoadableProperty()
            {
                Name = "level-colon",
                Slug = "Level_Colon",
                DataPath = modelPath + themeName
            },

            // Level Fan
            new LoadableProperty()
            {
                Name = "level-fan",
                Slug = "Level_Fan",
                DataPath = modelPath + themeName
            },

            // Level Gate
            new LoadableProperty()
            {
                Name = "level-gate",
                Slug = "Level_Gate",
                DataPath = modelPath + themeName
            },

            // Level Jump Pod
            new LoadableProperty()
            {
                Name = "level-jump-pod",
                Slug = "Level_JumpPod",
                DataPath = modelPath + themeName
            },

            // Level Laser
            new LoadableProperty()
            {
                Name = "level-laser",
                Slug = "Level_Laser",
                DataPath = modelPath + themeName
            },

            // Level Pump
            new LoadableProperty()
            {
                Name = "level-pump",
                Slug = "Level_Pump",
                DataPath = modelPath + themeName
            },

            // Level Valve
            new LoadableProperty()
            {
                Name = "level-valve",
                Slug = "Level_Valve",
                DataPath = modelPath + themeName
            }
        );
        // End: Adding properties
    }
}
