using UnityEngine;
using System.Collections;

public interface ILoadable : IGameProperty
{
	void LoadData();
	
	string DataPath { get; set; }
    string Slug { get; set; }
}