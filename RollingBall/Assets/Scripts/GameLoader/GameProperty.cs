﻿using UnityEngine;
using System.Collections;
using System;

public class GameProperty : IGameProperty
{
    private GameObject value;
    private string name;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public GameObject Value
    {
        get { return value; }
        set { this.value = value; }
    }
}
