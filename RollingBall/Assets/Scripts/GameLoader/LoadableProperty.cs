using UnityEngine;
using System.Collections;
using System;

public class LoadableProperty : ILoadable
{
    private string propName;        // Name of the propety
    private string dataPath;		// Path to the folder containing the resource to be loaded
    private string slug;            // File name of the resource file

    private GameObject objValue;    // Holds the loaded resource

    // IMPLEMENTATION: LoadData() for loading ILoadable from 'Resources folder'
    void ILoadable.LoadData()
	{
		// If the last index of DataPath is not '\'
		if (DataPath[DataPath.Length - 1] != '/')
		{
			DataPath += "/";		// Add '/' to DataPath
		}
		
		// Get value from resources folder with the given data path and slug
		Value = Resources.Load(DataPath + Slug) as GameObject;
	}

	// IMPLEMENTATION: Value of the loaded resource (A GameObject)
	public GameObject Value
	{
		get { return objValue; }
		set { objValue = value; }
	}
	
	// IMPLEMENTATION: The file name of the resource to be loaded
	public string Slug
	{
		get { return slug; }
		set { slug = value; }
	}
	
	// IMPLEMENTATION: Path to the folder that contains the resource to be loaded
	public string DataPath
	{
		get { return dataPath; }
		set { dataPath = value; }
	}

    // IMPLEMENTATION: Name of the property
    public string Name
    {
        get { return propName; }
        set { propName = value; }
    }
}