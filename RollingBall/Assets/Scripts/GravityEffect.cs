﻿using UnityEngine;
using System.Collections;

public class GravityEffect : MonoBehaviour {

    
    [SerializeField]
    private float force;

    private Rigidbody playerRigid;

    
    private void Start()
    {
        playerRigid = this.GetComponent<Rigidbody>();
    }

    private void OnTriggerStay(Collider col)
    {
        if (col.gameObject.CompareTag("Gate"))
        {
            playerRigid.AddForce(Vector3.down * force, ForceMode.Impulse);
        }
    }
}
